package com.bnn.mail.constant;

public class MessageConstant {

    public static final String COMMON_MESSAGES_PATH = "messages";

    /** {0} registered successful. */
    public static final String COMMON_MESSAGE_0001 = "common.message.0001";
    /** {0} modified successful. */
    public static final String COMMON_MESSAGE_0002 = "common.message.0002";
    /** {0} delete succesful. */
    public static final String COMMON_MESSAGE_0003 = "common.message.0003";
    /** {0} registered unsuccesful. */
    public static final String COMMON_MESSAGE_0004 = "common.message.0004";
    /** {0} modified unsuccesful. */
    public static final String COMMON_MESSAGE_0005 = "common.message.0005";
    /** {0} delete unsuccesful. */
    public static final String COMMON_MESSAGE_0006 = "common.message.0006";
    /** Confirmation Message. */
    public static final String COMMON_MESSAGE_0007 = "common.message.0007";
    /** Are you sure want to {0} this {1}?. */
    public static final String COMMON_MESSAGE_0008 = "common.message.0008";
    /** Not yet approved. */
    public static final String COMMON_MESSAGE_0009 = "common.message.0009";
    /** Approved. */
    public static final String COMMON_MESSAGE_0010 = "common.message.0010";


    /** {0} does not exist. */
    public static final String ERROR_MESSAGE_0001 = "error.message.0001";
    /** {0} is required. */
    public static final String ERROR_MESSAGE_0002 = "error.message.0002";
    /** {0} is not required. */
    public static final String ERROR_MESSAGE_0003 = "error.message.0003";
    /** Unhandled error occurred. */
    public static final String ERROR_MESSAGE_0004 = "error.message.0004";
    /** Password does not match. */
    public static final String ERROR_MESSAGE_0005 = "error.message.0005";
    /** Old password not match. */
    public static final String ERROR_MESSAGE_0006 = "error.message.0006";
    /** New Password and Re-Type Password doesn't match. */
    public static final String ERROR_MESSAGE_0007 = "error.message.0007";
    /** Account id not match with token. */
    public static final String ERROR_MESSAGE_0008 = "error.message.0008";


    /** Name is required. */
    public static final String VALIDATION_MESSAGE_0001 = "validation.message.0001";
    /** Username is required. */
    public static final String VALIDATION_MESSAGE_0002 = "validation.message.0002";
    /** Password is required. */
    public static final String VALIDATION_MESSAGE_0003 = "validation.message.0003";
    /** Gender is required. */
    public static final String VALIDATION_MESSAGE_0004 = "validation.message.0004";
    /** Invalid email address. */
    public static final String VALIDATION_MESSAGE_0005 = "validation.message.0005";
    /** Email address is already used. */
    public static final String VALIDATION_MESSAGE_0006 = "validation.message.0006";
    /** Old Password is required. */
    public static final String VALIDATION_MESSAGE_0007 = "validation.message.0007";
    /** New Password is required. */
    public static final String VALIDATION_MESSAGE_0008 = "validation.message.0008";
    /** Re-Type Password is required. */
    public static final String VALIDATION_MESSAGE_0009 = "validation.message.0009";

    /** Success Logout. */
    public static final String SUCCESS_LOGOUT = "Success Logout";


    /** Search. */
    public static final String COMMON_PAGE_0001 = "common.page.0001";
    /** Register. */
    public static final String COMMON_PAGE_0002 = "common.page.0002";
    /** Delete. */
    public static final String COMMON_PAGE_0003 = "common.page.0003";
    /** Modify. */
    public static final String COMMON_PAGE_0004 = "common.page.0004";
    /** Save. */
    public static final String COMMON_PAGE_0005 = "common.page.0005";
    /** Back. */
    public static final String COMMON_PAGE_0006 = "common.page.0006";
    /** #. */
    public static final String COMMON_PAGE_0007 = "common.page.0007";
    /** Created Date. */
    public static final String COMMON_PAGE_0008 = "common.page.0008";
    /** Updated Date. */
    public static final String COMMON_PAGE_0009 = "common.page.0009";
    /** Active. */
    public static final String COMMON_PAGE_0010 = "common.page.0010";
    /** Action. */
    public static final String COMMON_PAGE_0011 = "common.page.0011";
    /** Is active. */
    public static final String COMMON_PAGE_0012 = "common.page.0012";
    /** Yes */
    public static final String COMMON_PAGE_0013 = "common.page.0013";
    /** No */
    public static final String COMMON_PAGE_0014 = "common.page.0014";
    /** Male. */
    public static final String COMMON_PAGE_0015 = "common.page.0015";
    /** Female. */
    public static final String COMMON_PAGE_0016 = "common.page.0016";
    /** Account. */
    public static final String COMMON_PAGE_0017 = "common.page.0017";
    /** Please select one. */
    public static final String COMMON_PAGE_0018 = "common.page.0018";
    /** Email. */
    public static final String COMMON_PAGE_0019 = "common.page.0019";
    /** Date of birth. */
    public static final String COMMON_PAGE_0020 = "common.page.0020";
    /** Place of birth. */
    public static final String COMMON_PAGE_0021 = "common.page.0021";
    /** Gender. */
    public static final String COMMON_PAGE_0022 = "common.page.0022";
    /** Submit. */
    public static final String COMMON_PAGE_0023 = "common.page.0023";

    /** User */
    public static final String USER_PAGE_0001 = "user.page.0001";

}
