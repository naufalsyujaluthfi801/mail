package com.bnn.mail.constant;

public class DateConstant {
    public static String DATE_FORMAT_0001 = "yyyy-MM-dd";
    public static String DATE_FORMAT_0002 = "yyyy/MM/dd";
    public static String DATE_FORMAT_0003 = "dd-MM-yyyy";
    public static String DATE_FORMAT_0004 = "dd/MM/yyyy";
    public static String DATE_FORMAT_0005 = "MM-dd-yyyy";
    public static String DATE_FORMAT_0006 = "MM/dd/yyyy";
    public static String DATE_FORMAT_0007 = "dd MMMM yyyy";
    public static String DATE_FORMAT_0008 = "MMMM";
    public static String DATE_FORMAT_0009 = "yyMMddHH";
    public static String DATE_FORMAT_0010 = "dd MMMM";
    public static String DATE_FORMAT_0011 = "dd MMM";
    public static String DATE_FORMAT_0012 = "yyyyMMddHHmmss";
    public static String DATE_FORMAT_0013 = "dd/MM";
    public static String DATE_FORMAT_0014 = "yyyy";
    public static String DATE_FORMAT_0015 = "MMMM yyyy";
    public static String DATE_FORMAT_0016 = "dd MMM yyyy";
    public static String DATE_FORMAT_0017 = "dd-MMMM-yyyy";
    public static String DATE_FORMAT_0018 = "MM";
    public static String DATE_FORMAT_0019 = "MM/yyyy";
    public static String DATE_FORMAT_0020 = "MMM";
    public static String DATE_FORMAT_0021 = "yyyy-MM";
    public static String DATE_FORMAT_0022 = "MMMM-yyyy";
    public static String DATE_FORMAT_0023 = "dd/MM/yyyy HH:mm:ss";
    public static String DATE_FORMAT_0024 = "yyyyMMdd";
    public static String DATE_FORMAT_0025 = "yyyyMMddHHmmssSSS";
    public static String DATE_FORMAT_0026 = "yyyy-MM-dd HH:mm:ss";
    public static String DATE_FORMAT_0027 = "dd/MM/yyyy HH:mm";
    public static String DATE_FORMAT_0028 = "yyyyMMddHHmm";
    public static String DATE_FORMAT_0029 = "HH:mm";
}
