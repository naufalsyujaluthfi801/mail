package com.bnn.mail.constant;

public class ModuleSidebarConstant {
    /** Dashboard */
    public static final String SIDEBAR_0001 = "sidebar.0001";
    /** Persuratan */
    public static final String SIDEBAR_0002 = "sidebar.0002";
    /** MasukkanSurat */
    public static final String SIDEBAR_0003 = "sidebar.0003";
}
