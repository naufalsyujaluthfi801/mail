package com.bnn.mail.constant;

import javax.servlet.http.PushBuilder;

public class CommonConstant {
    public static final int FAILED_RESPONSE_CODE = 0;
    public static final int SUCCESS_RESPONSE_CODE = 1;

    public static final String FAILED_RESPONSE_MESSAGE = "Failed";
    public static final String SUCCESS_RESPONSE_MESSAGE = "Success";

    public static final boolean FALSE_MESSAGE = false;
    public static final boolean TRUE_MESSAGE = true;

    public static final String EXCEPTION = "EXCEPTION: ";

    public static final int STATUS_APPROVE_NOT_YET_APPROVED = 0;
    public static final int STATUS_APPROVE_APPROVED = 1;
    public static final String STATUS_APPROVE_NOT_YET_APPROVED_DESC = "Belum disetujui";
    public static final String STATUS_APPROVE_APPROVED_DESC = "Sudah disetujui";

    public static final String DISPOSITION_CATEGORY_TATA_USAHA = "TATA USAHA";
    public static final String DISPOSITION_CATEGORY_P2M = "P2M";
    public static final String DISPOSITION_CATEGORY_REHABILITASI = "REHABILITASI";
    public static final String DISPOSITION_CATEGORY_BRANTAS = "BRANTAS";
    public static final String DISPOSITION_CATEGORY_TATA_USAHA_DESC = "Tata Usaha";
    public static final String DISPOSITION_CATEGORY_P2M_DESC = "p2m";
    public static final String DISPOSITION_CATEGORY_REHABILITASI_DESC = "Rehabilitasi";
    public static final String DISPOSITION_CATEGORY_BRANTAS_DESC = "Brantas";


}
