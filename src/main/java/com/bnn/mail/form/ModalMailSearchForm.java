package com.bnn.mail.form;

import lombok.Data;

@Data
public class ModalMailSearchForm {
    private String date;

    public ModalMailSearchForm() {
    }
}
