package com.bnn.mail.form;

import lombok.Data;

@Data
public class GeneralSearchForm {
    private Integer currentPage;
    private int totalPages;
    private int limit = 10;
    private long totalItems;
}
