package com.bnn.mail.form;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModalMailForm {
  private String id;
  private String noSurat;
  private String tanggalSurat;
  private String tanggalDiterima;
  private String tujuanSurat;
  private String asalSurat;
  private String keterangan;
  private String perihalSurat;
  private String lampiran;
  private String statusApproveDesc;
  private int statusApprove;
  private String lainnya;
  private MultipartFile file;
  private String dispositionCategory;
}
