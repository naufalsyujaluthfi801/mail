package com.bnn.mail.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.File;
import java.time.Instant;

@Data
@NoArgsConstructor
@Table(name = "m_upload")
@Entity
public class MUpload {
    @Id
    @Size(max = 50)
    @Column(name = "id", nullable = false, length = 50)
    private String id;

    @NotNull
    @Column(name = "filename", nullable = false)
    private String filename;

    @NotNull
    @Column(name = "filepath", nullable = false)
    private String filepath;

    @NotNull
    @Column(name = "filesize", nullable = false)
    private Long filesize;

    @Size(max = 100)
    @NotNull
    @Column(name = "filetype", nullable = false, length = 100)
    private String filetype;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "update_date")
    private Instant updateDate;

    @Size(max = 50)
    @Column(name = "update_by", length = 50)
    private String updateBy;
}
