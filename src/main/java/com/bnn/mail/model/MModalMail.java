package com.bnn.mail.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_modal_mail")
public class MModalMail {
    @Id
    @Size(max = 50)
    @Column(name = "id", nullable = false, length = 50)
    private String id;

    @Size(max = 50)
    @NotNull
    @Column(name = "no_surat", nullable = false, length = 50)
    private String noSurat;

    @Size(max = 50)
    @Column(name = "tanggal_surat", nullable = false, length = 50)
    private String tanggalSurat;

    @Size(max = 50)
    @Column(name = "tanggal_diterima", nullable = false, length = 50)
    private String tanggalDiterima;

    @Column(name = "date")
    private Date date;

    @NotNull
    @Column(name = "asal_surat", nullable = false)
    private String asalSurat;

    @NotNull
    @Column(name = "tujuan_surat", nullable = false)
    private String tujuanSurat;

    @NotNull
    @Column(name = "keterangan", nullable = false)
    private String keterangan;

    @NotNull
    @Column(name = "perihal", nullable = false)
    private String perihalSurat;

    @NotNull
    @Column(name = "lampiran", nullable = false)
    private String lampiran;

    @Column(name = "lainnya", nullable = false)
    private String lainnya;

    @Size(max = 50)
    @NotNull
    @Column(name = "upload_id", nullable = false, length = 50)
    private String uploadId;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive = true;

    @NotNull
    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted = false;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Size(max = 50)
    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @NotNull
    @Column(name = "status_approve", nullable = false)
    private int statusApprove;

    @Size (max = 50)
    @Column(name = "disposisi_kategori")
    private String disposisiKategori;

}
