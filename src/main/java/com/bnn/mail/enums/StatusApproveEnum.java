package com.bnn.mail.enums;

public enum StatusApproveEnum {
    NOT_YET_APPROVED(0, "Belum disetujui"),
    APPROVED(1, "Sudah disetujui");

    private int code;
    private String name;

    StatusApproveEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static StatusApproveEnum fromCode(int code) {
        for (StatusApproveEnum field : StatusApproveEnum.values()) {
            if (field.getCode() == code) {
                return field;
            }
        }
        return null;
    }
}
