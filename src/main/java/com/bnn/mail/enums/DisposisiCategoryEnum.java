package com.bnn.mail.enums;

import com.bnn.mail.constant.CommonConstant;

public enum DisposisiCategoryEnum {
    CATEGORY_TATA_USAHA(CommonConstant.DISPOSITION_CATEGORY_TATA_USAHA, "Seksi Tata Usaha"),
    CATEGORY_P2M(CommonConstant.DISPOSITION_CATEGORY_P2M, "Seksi P2M"),
    CATEGORY_REHABILITASI(CommonConstant.DISPOSITION_CATEGORY_REHABILITASI, "Seksi Rehabilitasi"),
    CATEGORY_BRANTAS(CommonConstant.DISPOSITION_CATEGORY_BRANTAS, "Seksi Brantas");

    private String code;
    private String name;

    DisposisiCategoryEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static DisposisiCategoryEnum fromCode(String code) {
        for (DisposisiCategoryEnum field : DisposisiCategoryEnum.values()) {
            if (field.getCode() == code) {
                return field;
            }
        }
        return null;
    }
}
