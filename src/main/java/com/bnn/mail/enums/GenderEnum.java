package com.bnn.mail.enums;

public enum GenderEnum {
    MALE("M", "Male"),
    FEMALE("F", "Female");

    private String code;
    private String name;

    GenderEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static GenderEnum fromCode(String code) {
        for (GenderEnum field : GenderEnum.values()) {
            if (field.getCode().equals(code)) {
                return field;
            }
        }
        return null;
    }

}

