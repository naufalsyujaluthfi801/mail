package com.bnn.mail.enums;

import com.bnn.mail.constant.MessageConstant;
import com.bnn.mail.utils.MessageUtils;

public enum IsActiveEnum {
    TRUE(true, "Yes"),
    FALSE(false, "No");

    private boolean key;
    private String name;

    IsActiveEnum(boolean key, String name) {
        this.key = key;
        this.name = name;
    }

    public boolean getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static IsActiveEnum fromKey(boolean key) {
        for (IsActiveEnum field : IsActiveEnum.values()) {
            if (field.getKey() == key) {
                return field;
            }
        }
        return null;
    }

}

