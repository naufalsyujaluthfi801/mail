package com.bnn.mail.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

@Slf4j
public class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... args) {
        log.error("Method name: {}" +
                "----- args: {} ----" +
                " error: {}", method.getName(), args, ex.getMessage());
        ex.printStackTrace();
    }

}
