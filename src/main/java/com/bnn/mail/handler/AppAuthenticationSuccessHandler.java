package com.bnn.mail.handler;

import com.bnn.mail.dto.UserSessionDto;
import com.bnn.mail.helper.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AppAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    private AuthenticationHelper authenticationHelper;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        try {
            this.setUserSession(request);
            super.onAuthenticationSuccess(request, response, authentication);
        } catch (ServletException e) {
            redirectStrategy.sendRedirect(request, response, "/dashboard");
        }
    }

    private void setUserSession(HttpServletRequest request) {
        UserSessionDto userSession = this.authenticationHelper.getUserSession();
        if (userSession == null) {
            setDefaultTargetUrl("/login");
        } else {
            HttpSession httpSession = request.getSession(true);
            httpSession.setAttribute("userSession", userSession);
        }
    }
}
