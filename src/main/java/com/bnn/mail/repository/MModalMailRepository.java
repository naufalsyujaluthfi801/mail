package com.bnn.mail.repository;

import com.bnn.mail.model.MModalMail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface MModalMailRepository extends JpaRepository<MModalMail, String> {
    @Query(value = "SELECT m FROM MModalMail m WHERE m.date =:date")
    List<MModalMail> finfByMailDate (Date date);

    @Query(value = "SELECT m FROM MModalMail m WHERE m.date = :date AND m.isActive = true AND m.isDeleted = false")
    List<MModalMail> findByIsActiveAndDeletedModalMail(@Param("date") Date date);

    @Query(value = "SELECT m FROM MModalMail m WHERE m.statusApprove =:statusApprove")
    List<MModalMail> findAllByMailNeededApproveByHead(@Param("statusApprove") int statusApprove);

    @Query(value = "SELECT m FROM MModalMail m WHERE m.statusApprove =:statusApprove")
    List<MModalMail> findAllByMailApproveByHead(@Param("statusApprove") int statusApprove);
}
