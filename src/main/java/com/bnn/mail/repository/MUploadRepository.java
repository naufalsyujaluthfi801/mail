package com.bnn.mail.repository;

import com.bnn.mail.model.MUpload;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MUploadRepository extends JpaRepository <MUpload, String> {
}
