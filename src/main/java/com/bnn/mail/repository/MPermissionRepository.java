package com.bnn.mail.repository;

import com.bnn.mail.model.MPermission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MPermissionRepository extends JpaRepository<MPermission, String> {
}
