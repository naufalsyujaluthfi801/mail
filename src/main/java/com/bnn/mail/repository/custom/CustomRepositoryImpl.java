package com.bnn.mail.repository.custom;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class CustomRepositoryImpl implements CustomRepository {

    private final EntityManager entityManager;

    public CustomRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
