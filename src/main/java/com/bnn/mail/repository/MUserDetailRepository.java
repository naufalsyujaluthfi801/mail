package com.bnn.mail.repository;

import com.bnn.mail.model.MUserDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MUserDetailRepository extends JpaRepository<MUserDetail, String> {
}
