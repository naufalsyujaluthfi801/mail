package com.bnn.mail.repository;

import com.bnn.mail.model.MRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface MRoleRepository extends JpaRepository<MRole, String> {
    @Query(value = "SELECT * FROM m_role limit 1", nativeQuery = true)
    MRole findOne ();
}
