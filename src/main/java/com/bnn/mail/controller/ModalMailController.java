package com.bnn.mail.controller;


import com.bnn.mail.dto.UserSessionDto;
import com.bnn.mail.dto.request.*;
import com.bnn.mail.dto.response.*;
import com.bnn.mail.form.ModalMailForm;
import com.bnn.mail.form.ModalMailSearchForm;
import com.bnn.mail.service.MModalMailService;
import com.bnn.mail.utils.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("mail")
@SessionAttributes(types = ModalMailSearchForm.class)
public class ModalMailController {
    private final MModalMailService mModalMailService;

    public ModalMailController(MModalMailService mModalMailService) {
        this.mModalMailService = mModalMailService;
    }

    @ModelAttribute
    public ModalMailForm setupModalMailForm() {
        return new ModalMailForm();
    }

    @ModelAttribute
    public ModalMailSearchForm setupModalMailSearchForm() {
        return new ModalMailSearchForm();
    }

    @GetMapping("")
    public String getViewModalMail(@ModelAttribute ModalMailSearchForm modalMailSearchForm, Model model) {
        ViewModalMailRequest request = new ViewModalMailRequest();
        request.setDate(modalMailSearchForm.getDate());
        ViewModalMailResponse viewModalMailResponse = this.mModalMailService.viewModalMail(request);
        if (!viewModalMailResponse.getIsSuccess()) {
            model.addAttribute("message", viewModalMailResponse.getMessage());
            model.addAttribute("isSuccess", viewModalMailResponse.getIsSuccess());
        } else {
            model.addAttribute("data", viewModalMailResponse.getData());
        }
        return "mail/addsurat";
    }

    @PostMapping
    public String processSearchModalMail(ModalMailSearchForm modalMailSearchForm) {
        return "redirect:/modalMail";
    }

    @GetMapping("registerMail")
    public String getModalMail() {
        return "mail/addsurat";
    }

    @PostMapping("registerMail")
    public String processTambahSurat (@ModelAttribute @Valid ModalMailForm modalMailForm, BindingResult result, Model model, RedirectAttributes redirectAttributes, HttpSession httpSession) {
        if (result.hasErrors()) {
            return "mail/addsurat";
        } else {
            UserSessionDto userSessionDto = (UserSessionDto) httpSession.getAttribute("userSession");
            ModalMailRequest request = new ModalMailRequest();
            request.setUserSession(userSessionDto);
            request.setTanggalSurat(modalMailForm.getTanggalSurat());
            request.setTanggalDiterima(modalMailForm.getTanggalDiterima());
            request.setNoSurat(modalMailForm.getNoSurat());
            request.setAsalSurat(modalMailForm.getAsalSurat());
            request.setTujuanSurat(modalMailForm.getTujuanSurat());
            request.setKeterangan(modalMailForm.getKeterangan());
            request.setPerihalSurat(modalMailForm.getPerihalSurat());
            request.setLampiran(modalMailForm.getLampiran());
            request.setLainnya(modalMailForm.getLainnya());
            request.setFile(modalMailForm.getFile());
            ModalMailResponse response = this.mModalMailService.insertMail(request);
            if (!response.getIsSuccess()) {
                model.addAttribute("isSuccess", response.getIsSuccess());
                model.addAttribute("message", response.getMessage());
                return "mail/addsurat";
            } else {
                redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
                redirectAttributes.addFlashAttribute("message", response.getMessage());
                return "redirect:/mail";
            }
        }
    }

    @GetMapping("acc-laporan")
    public String getViewModalMailNeededApprovedByHead(Model model) {
        ViewModalMailNeededApprovedByHeadRequest request = new ViewModalMailNeededApprovedByHeadRequest();
        ViewModalMailNeededApprovedByHeadResponse response = this.mModalMailService.viewMailNeededApprovedByHead(request);
        if (!response.getIsSuccess()) {
            model.addAttribute("message", response.getMessage());
            model.addAttribute("isSuccess", response.getIsSuccess());
        } else {
            model.addAttribute("data", response.getData());
            model.addAttribute("dispositionCategoryOptions", CommonUtils.getDispositionCategoryOptions());
        }
        return "laporan/accLaporan";
    }

    @PostMapping("modify")
    public String processModifyModalMailApproveByHead(@ModelAttribute ModalMailForm modalMailForm, Model model, RedirectAttributes redirectAttributes) {
//        LoadModalMailNeededApprovedByHeadRequest request = new LoadModalMailNeededApprovedByHeadRequest(id);
//        LoadModalMailNeededApprovedByHeadResponse response = this.mModalMailService.loadModalMailNeededApprovedByHead(request);
//        if (!response.getIsSuccess()){
//            redirectAttributes.addFlashAttribute("isSuccess", response.getIsSuccess());
//            redirectAttributes.addFlashAttribute("message", response.getMessage());
//            return "redirect:/laporan";
//        }
//        model.addAttribute("data", response.getData());
        return "laporan/modifyModalAccLaporan";
    }

    @GetMapping("view-hasilLaporan")
    public String getViewModalMailApprovedByHead(Model model){
        ViewModalMailApprovedByHeadRequest request = new ViewModalMailApprovedByHeadRequest();
        ViewModalMailApprovedByHeadResponse response = this.mModalMailService.viewModalMailApprovedByHead(request);
        if (!response.getIsSuccess()) {
            model.addAttribute("message", response.getMessage());
            model.addAttribute("isSuccess", response.getIsSuccess());
        } else {
            model.addAttribute("dataTataUsaha", response.getModalMailDtoTataUsahaList());
            model.addAttribute("dataP2M", response.getModalMailDtoP2MList());
            model.addAttribute("dataRehabilitasi", response.getModalMailDtoRehabilitasiList());
            model.addAttribute("dataBrantas", response.getModalMailDtoBrantasList());
        }
        return "laporan/hasilLaporan";
    }
}
