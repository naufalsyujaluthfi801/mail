package com.bnn.mail.controller;


import com.bnn.mail.dto.FileDto;
import com.bnn.mail.service.MUploadService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Controller
@RequestMapping("file")
public class FileController {
    private final MUploadService mUploadService;

    public FileController(MUploadService mUploadService) {
        this.mUploadService = mUploadService;
    }

    @GetMapping("view/{uploadId}")
    public void viewFile(@PathVariable("uploadId") String uploadId, HttpServletResponse httpServletResponse) throws IOException {
        FileDto fileDto = this.mUploadService.viewByUploadId(uploadId);
        InputStream inputStream = new FileInputStream(fileDto.getFile());
        if(inputStream != null){
            httpServletResponse.setContentType(fileDto.getMimetype());
            OutputStream outputStream = httpServletResponse.getOutputStream();
            byte[] buffer = new byte[1024];
            int byteRead;
            while ((byteRead=inputStream.read(buffer))!= -1){
                outputStream.write(buffer, 0,byteRead);
            }
            outputStream.flush();
            outputStream.close();
        }
    }

    @GetMapping("download/{uploadId}")
    public void downloadFile(@PathVariable("uploadId") String uploadId, HttpServletResponse httpServletResponse ) throws IOException {
        FileDto fileDto = this.mUploadService.viewByUploadId(uploadId);
        InputStream inputStream = new FileInputStream(fileDto.getFile());
        if(inputStream != null){
            httpServletResponse.setContentType(fileDto.getMimetype());
            httpServletResponse.setHeader("Content-Disposition","attachment; filename="+fileDto.getFilename());
            OutputStream outputStream = httpServletResponse.getOutputStream();
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead=inputStream.read(buffer))!= -1){
                outputStream.write(buffer,0,bytesRead);
            }
            outputStream.flush();
            outputStream.close();
        }
    }
}
