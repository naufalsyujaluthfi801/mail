package com.bnn.mail.controller;


import com.bnn.mail.dto.request.SignUpRequest;
import com.bnn.mail.dto.response.SignupResponse;
import com.bnn.mail.form.SignupForm;
import com.bnn.mail.helper.AuthenticationHelper;
import com.bnn.mail.service.MUserService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class WelcomeController {

    private final MUserService mUserService;
    private final AuthenticationHelper authenticationHelper;

    public WelcomeController(MUserService mUserService, AuthenticationHelper authenticationHelper) {
        this.mUserService = mUserService;
        this.authenticationHelper = authenticationHelper;
    }
@ModelAttribute
public SignupForm setupSignupForm() {
        return  new SignupForm();
    }

    @GetMapping("/login")
    public String loadLoginPage() {
        boolean isAuthenticated = this.authenticationHelper.isAuthenticated();
        if (isAuthenticated) {
            return "redirect:/dashboard";
        } else {
            return "welcome/login";
        }
    }

    @GetMapping("/signup")
    public String getSignUp() {
        boolean isAuthenticated = this.authenticationHelper.isAuthenticated();
        if (isAuthenticated) {
            return "redirect:/";
        } else {
            return "welcome/signup";
        }
    }

    @PostMapping("/signup")
    public String submitSignUp(@Validated @ModelAttribute SignupForm signupForm){
        SignUpRequest request = new SignUpRequest(signupForm);
        SignupResponse response = this.mUserService.insertUser(request);
            return "redirect:/login";
    }

}
