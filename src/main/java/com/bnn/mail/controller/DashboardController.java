package com.bnn.mail.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Controller
@RequestMapping
public class DashboardController {

    @GetMapping
    public String loadHomePage() {
        return "redirect:/dashboard";
    }

    @GetMapping("/dashboard")
    public String loadDashboardPage(HttpServletRequest request) {
        Locale currentLocal = request.getLocale();
        String countryCode = currentLocal.getCountry();
        String countryName = currentLocal.getDisplayName();
        String langCode = currentLocal.getLanguage();
        String langName = currentLocal.getDisplayLanguage();
        return "dashboard/index";
    }

}
