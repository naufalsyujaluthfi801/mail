package com.bnn.mail.controller;

import com.bnn.mail.constant.MessageConstant;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
public class MessageSourceController {

    @GetMapping(value = "msgSource.js", produces = "application/javascript")
    public String getMsgSource(HttpServletRequest request, HttpServletResponse response, Model model) {
        Locale locale = RequestContextUtils.getLocale(request);
        ResourceBundle bundle = ResourceBundle.getBundle(MessageConstant.COMMON_MESSAGES_PATH, locale);
        response.setContentType("application/javascript");
        SortedMap<String, String> messages = new TreeMap<>();
        for (Iterator<String> it = bundle.getKeys().asIterator(); it.hasNext(); ) {
            String key = it.next();
            String value = bundle.getString(key);
            messages.put(key, value);
        }
        model.addAttribute("messages", messages);
        return "message/msgSource.js";
    }

}
