package com.bnn.mail.helper;

import com.bnn.mail.dto.UserSessionDto;
import com.bnn.mail.model.MUser;
import com.bnn.mail.repository.MUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class AuthenticationHelper {

    private final MUserRepository userRepository;

    public AuthenticationHelper(MUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserSessionDto getUserSession() {
        UserSessionDto userSession = new UserSessionDto();
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
            List<String> authorityList = new ArrayList<String>();
            for (GrantedAuthority a : authorities) {
                authorityList.add(a.getAuthority());
            }
            MUser user = this.userRepository.findMUserByUsername(username);
            userSession = new UserSessionDto(user, authorityList);userSession = new UserSessionDto();
            log.info("Get user session successful, result: {}", userSession);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Get user session failed, message: {}", e);
        }
        return userSession;
    }

    public boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || AnonymousAuthenticationToken.class.
                isAssignableFrom(authentication.getClass())) {
            return false;
        }
        return authentication.isAuthenticated();
    }

}
