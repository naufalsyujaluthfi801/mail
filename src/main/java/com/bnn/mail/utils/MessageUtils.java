package com.bnn.mail.utils;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageUtils {

    private static final String PROPERTIES_NAME = "messages";

    private static Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }

    public static String getMessageString(String messageCode) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(PROPERTIES_NAME, getLocale());
        String result = resourceBundle.getString(messageCode);
        return result;
    }

    public static String getMessageString(String messageCode, String... param) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(PROPERTIES_NAME, getLocale());
        String result = resourceBundle.getString(messageCode);
        for (int i = 0; i < param.length; i++) {
            result = (result).replace("{" + i + "}", param[i]);
        }
        return result;
    }

}