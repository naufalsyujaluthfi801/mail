package com.bnn.mail.utils;

import com.bnn.mail.constant.DateConstant;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DateUtils {
    public static Date getMonthAndDayByDay(Short param) {
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Date today = new Date();
        LocalDate todayLocal = today.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String dateString = getParam(param);
        String monthString = getParam((short) todayLocal.getMonthValue());
        String date = todayLocal.getYear() + "-" + monthString + "-" + dateString;
        LocalDate result = LocalDate.parse(date, pattern);
        return Date.from(result.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date stringToDateUsingSimpleDateFormat(String pattern, String param) throws ParseException {
        Date result = null;
        if (!StringUtils.isBlank(param)) {
            DateFormat sdf = new SimpleDateFormat(pattern);
            sdf.setLenient(false);
            result = sdf.parse(param);
        }
        return result;
    }

    public static String dateToStringUsingSimpleDateFormat(String pattern, Date param) {
        String result = "";
        if (param != null) {
            DateFormat sdf = new SimpleDateFormat(pattern);
            result = sdf.format(param);
        }
        return result;
    }

    public static Date stringToDateUsingDateTimeFormatter(String pattern, String param) {
        Date result = null;
        if (!StringUtils.isBlank(param)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            LocalDate localDate = LocalDate.parse(param, formatter);
            result = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
        return result;
    }

    public static String dateToStringUsingDateTimeFormatter(String pattern, Date param) {
        String result = "";
        if (param != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            LocalDate localDate = param.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            result = localDate.format(formatter);
        }
        return result;
    }

    public static Date stringMonthYearOnlyToDateUsingDateTimeFormatter(String pattern, String param) {
        Date result = null;
        if (!StringUtils.isBlank(param)) {
            Integer day = LocalDate.now().getDayOfMonth();
            String dayString = getParam(day.shortValue());
            param = dayString + " " + param;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            LocalDate localDate = LocalDate.parse(param, formatter);
            result = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
        return result;
    }

    public static Date getDateFromMonthAndYearUsingJodaTime(Short monthParam, org.joda.time.LocalDate localDateParam) {
        String outputStr = "01" + "/" + getParam(monthParam) + "/" + localDateParam.getYear();
        Date output = stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, outputStr);
        return output;
    }

    public static Date getDateFromMonthAndYearUsingUtilDate(Short monthParam, Date date) {
        org.joda.time.LocalDate localDateParam = new org.joda.time.LocalDate(date);
        String outputStr = "01" + "/" + getParam(monthParam) + "/" + localDateParam.getYear();
        Date output = stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, outputStr);
        return output;
    }

    public static String getParam(Short param) {
        String result = "";
        if (param.toString().length() <= 1) {
            result = "0" + param;
        } else {
            result = param.toString();
        }
        return result;
    }

    public static String localDateToStringUsingDateTimeFormatter(String pattern, LocalDate param) {
        String result = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        result = param.format(formatter);
        return result;
    }

    public static LocalDate stringToLocalDateUsingDateTimeFormatter(String pattern, String param) {
        LocalDate result = null;
        if (!StringUtils.isBlank(param)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            result = LocalDate.parse(param, formatter);
        }
        return result;
    }

    public static Date changeYearUsingJodaTime(int yearParam, Date dateParam) {
        String dateStr = dateToStringUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0013, dateParam);
        dateStr = dateStr + "/" + yearParam;
        Date output = stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, dateStr);
        return output;
    }

    public static Date changeDay(int dayParam, Date dateParam) {
        String dateStr = dateToStringUsingSimpleDateFormat(DateConstant.DATE_FORMAT_0004, dateParam);
        String[] temp = dateStr.split("/");
        String outputStr = getParam((short) dayParam) + "/" + temp[1] + "/" + temp[2];
        Date output = stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, outputStr);
        return output;
    }

    public static int getCurrentYearFromDate(Date date) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(date);
        return localDate.getYear();
    }

    public static int getLastYearFromDate(Date date) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(date);
        return localDate.getYear() - 1;
    }

    public static Date changeYearWithLastYearUsingJodaTime(Date dateParam) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(dateParam);
        String dateStr = dateToStringUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0013, dateParam);
        dateStr = dateStr + "/" + (localDate.getYear() - 1);
        Date output = stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, dateStr);
        return output;
    }

    public static List<String> getMonhtsByStartDateAndEndDate(Date startDate, Date endDate) {
        List<String> result = new ArrayList<>();
        org.joda.time.LocalDate localStartDate = new org.joda.time.LocalDate(startDate);
        org.joda.time.LocalDate localEndDate = new org.joda.time.LocalDate(endDate);
        while (localStartDate.isBefore(localEndDate) || localStartDate.equals(localEndDate)) {
            String month = localStartDate.toString(DateConstant.DATE_FORMAT_0020);
            result.add(month);
            localStartDate = localStartDate.plus(Period.months(1));
        }
        return result;
    }

    public static List<String> getMonthsByStartDateAndEndDateByDateConstant(Date startDate, Date endDate, String dateConstant) {
        List<String> result = new ArrayList<String>();
        org.joda.time.LocalDate localStartDate = new org.joda.time.LocalDate(startDate);
        org.joda.time.LocalDate localEndDate = new org.joda.time.LocalDate(endDate);
        while (localStartDate.isBefore(localEndDate) || localStartDate.equals(localEndDate)) {
            String month = localStartDate.toString(dateConstant);
            result.add(month);
            localStartDate = localStartDate.plus(Period.months(1));

        }
        return result;
    }

    public static Date changeYearUsingJodaTime(Date dateParam, int plusYear) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(dateParam);
        String dateStr = dateToStringUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0013, dateParam);
        dateStr = dateStr + "/" + (localDate.getYear() + plusYear);
        Date output = stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, dateStr);
        return output;
    }

    public static int getMonthIntegerFromDate(Date date) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(date);
        return localDate.getMonthOfYear();
    }

    public static int getMonthIntegerFromDate(String dateString) throws ParseException {
        Date date= stringToDateUsingSimpleDateFormat(DateConstant.DATE_FORMAT_0001, dateString);
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(date);
        return localDate.getMonthOfYear();
    }

    public static String localDateJodaTimeToStringUsingDateTimeFormatter(String pattern, org.joda.time.LocalDate param) {
        String result = null;
        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        result = param.toString(formatter);
        return result;
    }

    public static org.joda.time.LocalDate stringToLocalDateJodaTimeUsingDateTimeFormatter(String pattern, String param) {
        org.joda.time.LocalDate result = null;
        if (!StringUtils.isBlank(param)) {
            result = org.joda.time.LocalDate.parse(param, DateTimeFormat.forPattern(param));
        }
        return result;
    }

    public static Date addMonthDate(Date input, int month) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(input);
        Date result = localDate.plusMonths(month).toDate();
        return result;
    }

    public static Date substractMonthDate(Date input, int month) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(input);
        Date result = localDate.minusMonths(month).toDate();
        return result;
    }

    public static Date getFirstDayOfMonthUsingJodaTime(Date dateParam) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(dateParam);
        org.joda.time.LocalDate localDateFirstDayOfMonth = localDate.dayOfMonth().withMinimumValue();
        Date output = localDateFirstDayOfMonth.toDate();
        return output;
    }

    public static Date getLastDayOfMonthUsingJodaTime(Date dateParam) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(dateParam);
        org.joda.time.LocalDate localDateLastDayOfMonth = localDate.dayOfMonth().withMaximumValue();
        Date output = localDateLastDayOfMonth.toDate();
        return output;
    }

    public static Date clearTimeInDate(Date input) {
        if(input != null) {
            String date = DateUtils.dateToStringUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, input);
            return DateUtils.stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, date);
        }
        return null;
    }

    public static Date setTimeInDate(Date input) {
        if(input != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(input);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            return cal.getTime();
        }
        return null;
    }

    public static Date addDaysDate(Date input, int days) {
        org.joda.time.LocalDateTime localDateTime = new org.joda.time.LocalDateTime(input);
        Date result = localDateTime.plusDays(days).toDate();
        return result;
    }

    public static Date substractDaysDate(Date input, int days) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(input);
        Date result = localDate.minusDays(days).toDate();
        return result;
    }

    public static Date addYearDate(Date input, int year) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(input);
        Date result = localDate.plusYears(year).toDate();
        return result;
    }

    public static Date substractYearDate(Date input, int year) {
        org.joda.time.LocalDate localDate = new org.joda.time.LocalDate(input);
        Date result = localDate.minusYears(year).toDate();
        return result;
    }

    public static Date getFirstOfDateFromYear(String year) {
        String outputStr = "01" + "/01/" + year;
        Date output = stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, outputStr);
        return output;
    }

    public static Date getLastOfDateFromYear(String year) {
        String outputStr = "31" + "/12/" + year;
        Date output = stringToDateUsingDateTimeFormatter(DateConstant.DATE_FORMAT_0004, outputStr);
        return output;
    }

    public static List<Date> getRangePeriod(String from, String to, String param) throws ParseException {
        List<Date> rangePeriod = new LinkedList<Date>();
        String periodFrom = "01 "+ from;
        String periodTo = "01 "+ to;
        LocalDate start = DateUtils.stringToLocalDateUsingDateTimeFormatter(param, periodFrom);
        LocalDate end = DateUtils.stringToLocalDateUsingDateTimeFormatter(param, periodTo);
        rangePeriod.add(java.sql.Date.valueOf(start));
        rangePeriod.add(java.sql.Date.valueOf(end));
        return rangePeriod;
    }

    public static String getSimpleMonthName(Integer month) {
        String[] months = {"Jan","Feb","Mar","Apl","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
        return months[month-1];
    }

    public static List<String> getFullMonths(String dateConstant) {
        List<String> result = new ArrayList<String>();
        org.joda.time.LocalDate localStartDate = new org.joda.time.LocalDate(getFirstOfDateFromYear("1990"));
        org.joda.time.LocalDate localEndDate = new org.joda.time.LocalDate(getLastOfDateFromYear("1990"));
        while (localStartDate.isBefore(localEndDate) || localStartDate.equals(localEndDate)) {
            String month = localStartDate.toString(dateConstant);
            result.add(month);
            localStartDate = localStartDate.plus(Period.months(1));
        }
        return result;
    }

    public static String changeDateFormat(String date, String oldFormat, String newFormat) {
        try {
            SimpleDateFormat oldSdf = new SimpleDateFormat(oldFormat);
            oldSdf.setLenient(false);

            Date result = oldSdf.parse(date);
            SimpleDateFormat newSdf = new SimpleDateFormat(newFormat);
            newSdf.setLenient(false);
            return newSdf.format(result);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
