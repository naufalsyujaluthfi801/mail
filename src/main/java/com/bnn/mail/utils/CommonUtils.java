package com.bnn.mail.utils;

import com.bnn.mail.constant.CommonConstant;
import com.bnn.mail.constant.MessageConstant;
import com.bnn.mail.dto.DisposisiCategoryDto;
import com.bnn.mail.dto.IsActiveDto;
import com.bnn.mail.dto.StatusApproveDto;
import com.bnn.mail.enums.DisposisiCategoryEnum;
import com.bnn.mail.enums.IsActiveEnum;
import com.bnn.mail.enums.StatusApproveEnum;
import org.apache.commons.io.FilenameUtils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommonUtils {
    public static List<IsActiveDto> getIsActiveOptions() {
        List<IsActiveDto> result = new ArrayList<>();
        List<IsActiveEnum> isActiveEnums = Arrays.asList(IsActiveEnum.TRUE, IsActiveEnum.FALSE);
        for (IsActiveEnum isActiveEnum: isActiveEnums) {
            String value;
            if (isActiveEnum.getKey())
                value = MessageUtils.getMessageString(MessageConstant.COMMON_PAGE_0013);
            else
                value = MessageUtils.getMessageString(MessageConstant.COMMON_PAGE_0014);
            IsActiveDto dto = new IsActiveDto(value, isActiveEnum.getKey());
            result.add(dto);
        }
        return result;
    }

    public static String getFilenameWithoutExtension(String filename){
        return FilenameUtils.removeExtension(filename);
    }

    public static String getExtensionFromFilename(String filename){
        return FilenameUtils.getExtension(filename);
    }

    public static List<StatusApproveDto> getStatusApproveOption() {
        List<StatusApproveDto> result = new ArrayList<>();
        List<StatusApproveEnum> statusApproveEnums = Arrays.asList(StatusApproveEnum.NOT_YET_APPROVED, StatusApproveEnum.APPROVED);
        for (StatusApproveEnum statusApproveEnum: statusApproveEnums) {
            String value;
            if (statusApproveEnum.getCode() == CommonConstant.STATUS_APPROVE_NOT_YET_APPROVED)
                value = MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0009);
            else
                value = MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0010);
            StatusApproveDto dto = new StatusApproveDto(statusApproveEnum.getCode(), value);
            result.add(dto);
        }
        return result;
    }

    public static List<DisposisiCategoryDto> getDispositionCategoryOptions() {
        List<DisposisiCategoryDto> result = new ArrayList<>();
        List<DisposisiCategoryEnum> disposisiCategoryEnums = Arrays.asList(
                DisposisiCategoryEnum.CATEGORY_TATA_USAHA,
                DisposisiCategoryEnum.CATEGORY_P2M,
                DisposisiCategoryEnum.CATEGORY_REHABILITASI,
                DisposisiCategoryEnum.CATEGORY_BRANTAS);
        for (DisposisiCategoryEnum disposisiCategoryEnum: disposisiCategoryEnums) {
            DisposisiCategoryDto dto = new DisposisiCategoryDto(disposisiCategoryEnum.getCode(), disposisiCategoryEnum.getName());
            result.add(dto);
        }
        return result;
    }
}
