package com.bnn.mail.auth;

import com.bnn.mail.model.MUser;
import com.bnn.mail.repository.MUserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    private final MUserRepository mUserRepository;

    public CustomUserDetailsServiceImpl(MUserRepository mUserRepository) {
        this.mUserRepository = mUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MUser mUser = this.mUserRepository.findMUserByUsername(username);
        if (mUser != null) {
            List<GrantedAuthority> grantedAuthorities = this.getAuthorities(this.getPermissions(username));
            return new CustomUserDetails(mUser, grantedAuthorities);
        }
        throw new UsernameNotFoundException("User not available");
    }

    private List<String> getPermissions(String username) {
        return this.mUserRepository.findAllPermissionCodeByUsername(username);
    }

    private List<GrantedAuthority> getAuthorities(List<String> permissions) {
        List<GrantedAuthority> result = new ArrayList<GrantedAuthority>();
        for (String per : permissions) {
            result.add(new SimpleGrantedAuthority(per));
        }
        return result;
    }
}
