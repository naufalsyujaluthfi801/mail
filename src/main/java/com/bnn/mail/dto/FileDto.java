package com.bnn.mail.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;

@Data
@NoArgsConstructor
public class FileDto {
    private String mimetype;
    private String filename;
    private File file;
}
