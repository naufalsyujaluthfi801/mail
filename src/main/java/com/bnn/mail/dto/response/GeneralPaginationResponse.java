package com.bnn.mail.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneralPaginationResponse<T> {
    private String message;
    private Boolean isSuccess;
    private int totalPages;
    private long totalItems;
    private T data;
}
