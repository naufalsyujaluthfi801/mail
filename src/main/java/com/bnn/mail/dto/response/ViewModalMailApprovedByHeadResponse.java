package com.bnn.mail.dto.response;


import com.bnn.mail.dto.ModalMailDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ViewModalMailApprovedByHeadResponse extends GeneralResponse {
    List<ModalMailDto> modalMailDtoTataUsahaList;
    List<ModalMailDto> modalMailDtoP2MList;
    List<ModalMailDto> modalMailDtoRehabilitasiList;
    List<ModalMailDto> modalMailDtoBrantasList;
}
