package com.bnn.mail.dto.response;


import com.bnn.mail.dto.ModalMailDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ViewModalMailNeededApprovedByHeadResponse extends GeneralResponse{
    List<ModalMailDto> modalMailDtoList;
}
