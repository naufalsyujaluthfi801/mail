package com.bnn.mail.dto.response;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ViewModalMailResponse extends GeneralResponse {
}
