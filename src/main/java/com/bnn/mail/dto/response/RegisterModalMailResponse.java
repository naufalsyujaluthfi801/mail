package com.bnn.mail.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class RegisterModalMailResponse extends GeneralResponse{
}
