package com.bnn.mail.dto.response;

import com.bnn.mail.dto.request.ModalMailRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class ModalMailResponse extends GeneralResponse {
}
