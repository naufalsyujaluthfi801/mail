package com.bnn.mail.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModalMailDto {
    private String id;
    private String noSurat;
    private String tanggalSurat;
    private String tujuanSurat;
    private String tanggalDiterima;
    private String asalSurat;
    private String keterangan;
    private String perihalSurat;
    private String lampiran;
    private String statusApproveDesc;
    private int statusApprove;
    private String lainnya;
    private String uploadId;
}
