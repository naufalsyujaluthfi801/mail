package com.bnn.mail.dto;

import lombok.Data;

@Data
public class StatusApproveDto {
    private int code;
    private String description;

    public StatusApproveDto() {
    }

    public StatusApproveDto(int code, String description) {
        this.code = code;
        this.description = description;
    }
}
