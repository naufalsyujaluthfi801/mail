package com.bnn.mail.dto;

import com.bnn.mail.model.MUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSessionDto {
    private String userId;
    private String username;
    private String roleId;
    private String roleName;
    private List<String> authorities;

    public UserSessionDto(MUser user, List<String> authorities) {
        this.userId = user.getId();
        this.username = user.getUsername();
        this.roleId = user.getRole() != null ? user.getRole().getId() : null;
        this.roleName = user.getRole() != null ? user.getRole().getName() : null;
        this.authorities = authorities;
    }
}
