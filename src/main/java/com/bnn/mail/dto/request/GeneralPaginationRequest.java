package com.bnn.mail.dto.request;

import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@Data
public class GeneralPaginationRequest {
    private Integer offset;
    private Integer limit;
    private int page;
    private Pageable pageable;

    public GeneralPaginationRequest() {
    }

    public GeneralPaginationRequest(Integer limit, int page) {
        this.limit = limit;
        this.page = page;
        this.pageable = PageRequest.of(this.page - 1, this.limit);
    }
}
