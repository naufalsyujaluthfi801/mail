package com.bnn.mail.dto.request;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

@Data
@EqualsAndHashCode
public class DeleteModalMailRequest extends GeneralRequest{
    private String noSurat;
    private String tanggalSurat;
    private String tanggalDiterima;
    private String tujuanSurat;
    private String asalSurat;
    private String keterangan;
    private String perihalSurat;
    private String lampiran;
    private String status;
    private String lainnya;
    private MultipartFile file;

    public DeleteModalMailRequest() {
    }
}
