package com.bnn.mail.dto.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoadModalMailNeededApprovedByHeadRequest {
    private String id;
}
