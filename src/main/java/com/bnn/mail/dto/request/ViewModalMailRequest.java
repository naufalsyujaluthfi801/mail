package com.bnn.mail.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class ViewModalMailRequest extends GeneralRequest {
    private String date;
}
