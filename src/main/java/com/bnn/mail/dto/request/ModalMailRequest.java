package com.bnn.mail.dto.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;


@Data
@EqualsAndHashCode
public class ModalMailRequest extends GeneralRequest{
    private String noSurat;
    private String tanggalSurat;
    private String tanggalDiterima;
    private String asalSurat;
    private String tujuanSurat;
    private String keterangan;
    private String perihalSurat;
    private String lampiran;
    private String lainnya;
    private MultipartFile file;

    public ModalMailRequest() {
    }
}
