package com.bnn.mail.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

@Data
@EqualsAndHashCode
public class ModifyModalMailRequest extends GeneralRequest {
    private String noSurat;
    private String tanggalSurat;
    private String tanggalDiterima;
    private MultipartFile file;

    public ModifyModalMailRequest() {
    }
}
