package com.bnn.mail.dto.request;

import com.bnn.mail.form.SignupForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequest {
    private String roleId;
    private String username;
    private String password;

    public SignUpRequest(SignupForm form) {
        this.roleId = form.getRoleId();
        this.username = form.getUsername();
        this.password = form.getPassword();
    }
}
