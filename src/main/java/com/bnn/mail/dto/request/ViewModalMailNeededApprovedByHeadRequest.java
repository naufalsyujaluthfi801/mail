package com.bnn.mail.dto.request;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ViewModalMailNeededApprovedByHeadRequest extends GeneralRequest{
}
