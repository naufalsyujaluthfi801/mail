package com.bnn.mail.dto;

import lombok.Data;

@Data
public class DisposisiCategoryDto {
    private String code;
    private String description;

    public DisposisiCategoryDto() {
    }

    public DisposisiCategoryDto(String code, String description) {
        this.code = code;
        this.description = description;
    }
}
