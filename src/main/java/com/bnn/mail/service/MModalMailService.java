package com.bnn.mail.service;

import com.bnn.mail.dto.request.*;
import com.bnn.mail.dto.response.*;

public interface MModalMailService {
    ModalMailResponse insertMail(ModalMailRequest request);
    ViewModalMailResponse viewModalMail(ViewModalMailRequest request);
    ViewModalMailNeededApprovedByHeadResponse viewMailNeededApprovedByHead(ViewModalMailNeededApprovedByHeadRequest request);
    LoadModalMailNeededApprovedByHeadResponse loadModalMailNeededApprovedByHead(LoadModalMailNeededApprovedByHeadRequest request);
    ViewModalMailApprovedByHeadResponse viewModalMailApprovedByHead(ViewModalMailApprovedByHeadRequest request);
//    ModifyModalMailResponse modifyModalMail(ModifyModalMailRequest request);
//    LoadModifyModalMailResponse loadModify(LoadModifyModalMailRequest request);
//    DeleteModalMailResponse deleteModalMail(DeleteModalMailRequest request);



}
