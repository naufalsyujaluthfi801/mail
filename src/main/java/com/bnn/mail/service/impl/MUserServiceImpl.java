package com.bnn.mail.service.impl;

import com.bnn.mail.constant.CommonConstant;
import com.bnn.mail.constant.MessageConstant;
import com.bnn.mail.dto.request.SignUpRequest;
import com.bnn.mail.dto.response.SignupResponse;
import com.bnn.mail.model.MRole;
import com.bnn.mail.model.MUser;
import com.bnn.mail.repository.MRoleRepository;
import com.bnn.mail.repository.MUserRepository;
import com.bnn.mail.service.MUserService;
import com.bnn.mail.utils.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class MUserServiceImpl implements MUserService {

    private MUserRepository mUserRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private MRoleRepository mRoleRepository;

    @Autowired
    public MUserServiceImpl(MUserRepository mUserRepository, BCryptPasswordEncoder passwordEncoder, MRoleRepository mRoleRepository) {
        this.mUserRepository = mUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.mRoleRepository = mRoleRepository;
    }

    @Override
    public SignupResponse insertUser(SignUpRequest request) {
        SignupResponse response = new SignupResponse();
        try {
            boolean isExists = this.mUserRepository.existsMUserByUsername(request.getUsername());
            if (!isExists) {
                String password = passwordEncoder.encode(request.getPassword()).toString();
                MRole mRole = this.mRoleRepository.findOne();
                if (mRole == null){
                    throw new RuntimeException("Role Id Is Null");
                }
                MUser mUser = new MUser(request, password, mRole);
                this.mUserRepository.save(mUser);
                response.setIsSuccess(true);
                response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
            } else {
                response.setIsSuccess(false);
                response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
            }
        } catch (Exception e) {
            log.error(CommonConstant.EXCEPTION, e);
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
        }
        return response;
    }

}
