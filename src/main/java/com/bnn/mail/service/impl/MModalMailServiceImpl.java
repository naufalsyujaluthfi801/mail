package com.bnn.mail.service.impl;


import com.bnn.mail.constant.CommonConstant;
import com.bnn.mail.constant.MessageConstant;
import com.bnn.mail.constant.ModuleSidebarConstant;
import com.bnn.mail.dto.ModalMailDto;
import com.bnn.mail.dto.StatusApproveDto;
import com.bnn.mail.dto.request.*;
import com.bnn.mail.dto.response.*;
import com.bnn.mail.model.MModalMail;
import com.bnn.mail.model.MUpload;
import com.bnn.mail.repository.MModalMailRepository;
import com.bnn.mail.service.MModalMailService;
import com.bnn.mail.service.MUploadService;
import com.bnn.mail.utils.CommonUtils;
import com.bnn.mail.utils.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@Transactional
public class MModalMailServiceImpl implements MModalMailService {

    private final MModalMailRepository mModalMailRepository;
    private final MUploadService mUploadService;

    public MModalMailServiceImpl(MModalMailRepository mModalMailRepository, MUploadService mUploadService) {
        this.mModalMailRepository = mModalMailRepository;
        this.mUploadService = mUploadService;


    }

    @Override
    public ModalMailResponse insertMail(ModalMailRequest request) {
        ModalMailResponse response = new ModalMailResponse();
        try {
            MUpload mUpload = mUploadService.insertUpload(request.getFile(),request.getUserSession());
            MModalMail mModalMail = new MModalMail();
            mModalMail.setId(UUID.randomUUID().toString());
            mModalMail.setNoSurat(request.getNoSurat());
            mModalMail.setTanggalSurat(request.getTanggalSurat());
            mModalMail.setTanggalDiterima(request.getTanggalDiterima());
            mModalMail.setDate(new Date());
            mModalMail.setAsalSurat(request.getAsalSurat());
            mModalMail.setTujuanSurat(request.getTujuanSurat());
            mModalMail.setKeterangan(request.getKeterangan());
            mModalMail.setPerihalSurat(request.getPerihalSurat());
            mModalMail.setLampiran(request.getLampiran());
            mModalMail.setLainnya(request.getLainnya());
            mModalMail.setUploadId(mUpload.getId());
            mModalMail.setStatusApprove(CommonConstant.STATUS_APPROVE_NOT_YET_APPROVED);
            mModalMail.setIsActive(true);
            mModalMail.setIsDeleted(false);
            mModalMail.setCreatedDate(Instant.now());
            mModalMail.setCreatedBy(request.getUserSession().getUserId());
            this.mModalMailRepository.save(mModalMail);
            response.setIsSuccess(true);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0002)));
            log.info("Register Mail Succesfull, result{}", mModalMail);
        }catch (Exception e) {
            log.info("Register Mail failed, Message, result{}", e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(ModuleSidebarConstant.SIDEBAR_0002)));
        }
        return response;
        }

    @Override
    public ViewModalMailResponse viewModalMail(ViewModalMailRequest request) {
        ViewModalMailResponse viewModalMailResponse = new ViewModalMailResponse();
        try {
            Date today = new Date();
            List<MModalMail> mModalMails = this.mModalMailRepository.finfByMailDate(today);
            List<ModalMailDto> modalMailDtos = new ArrayList<>();
            List<StatusApproveDto> statusApproveDtos = CommonUtils.getStatusApproveOption();
            for (MModalMail modalMail:mModalMails){
                StatusApproveDto statusApproveDto = statusApproveDtos.stream().filter(item -> item.getCode() == modalMail.getStatusApprove()).findAny().orElse(null);
                ModalMailDto modalMailDto = new ModalMailDto();
                modalMailDto.setId(modalMail.getId());
                modalMailDto.setNoSurat(modalMail.getNoSurat());
                modalMailDto.setTanggalSurat(modalMail.getTanggalSurat());
                modalMailDto.setTanggalDiterima(modalMail.getTanggalDiterima());
                modalMailDto.setTujuanSurat(modalMail.getTujuanSurat());
                modalMailDto.setAsalSurat(modalMail.getAsalSurat());
                modalMailDto.setKeterangan(modalMail.getKeterangan());
                modalMailDto.setPerihalSurat(modalMail.getPerihalSurat());
                modalMailDto.setLampiran(modalMail.getLampiran());
                modalMailDto.setStatusApproveDesc(statusApproveDto.getDescription());
                modalMailDto.setStatusApprove(modalMail.getStatusApprove());
                modalMailDto.setLainnya(modalMail.getLainnya());
                modalMailDto.setUploadId(modalMail.getUploadId());
                modalMailDtos.add(modalMailDto);
            }
            viewModalMailResponse.setData(modalMailDtos);
            viewModalMailResponse.setIsSuccess(true);
            log.info("View Mail Succesfull", mModalMails);
        }catch (Exception e){
            log.error("View Mail Failed", e.getMessage());
            e.printStackTrace();
            viewModalMailResponse.setIsSuccess(false);
            viewModalMailResponse.setMessage(e.getMessage());
        }
        return viewModalMailResponse;
    }

    @Override
    public ViewModalMailNeededApprovedByHeadResponse viewMailNeededApprovedByHead(ViewModalMailNeededApprovedByHeadRequest request) {
        ViewModalMailNeededApprovedByHeadResponse response = new ViewModalMailNeededApprovedByHeadResponse();
        try {
            List<MModalMail> mModalMails = this.mModalMailRepository.findAllByMailNeededApproveByHead(CommonConstant.STATUS_APPROVE_NOT_YET_APPROVED);
            List<ModalMailDto> modalMailDtos = new ArrayList<>();
            List<StatusApproveDto> statusApproveDtos = CommonUtils.getStatusApproveOption();
            for (MModalMail modalMail:mModalMails){
                StatusApproveDto statusApproveDto = statusApproveDtos.stream().filter(item -> item.getCode() == modalMail.getStatusApprove()).findAny().orElse(null);
                ModalMailDto modalMailDto = new ModalMailDto();
                modalMailDto.setId(modalMail.getId());
                modalMailDto.setNoSurat(modalMail.getNoSurat());
                modalMailDto.setTanggalSurat(modalMail.getTanggalSurat());
                modalMailDto.setTanggalDiterima(modalMail.getTanggalDiterima());
                modalMailDto.setTujuanSurat(modalMail.getTujuanSurat());
                modalMailDto.setAsalSurat(modalMail.getAsalSurat());
                modalMailDto.setKeterangan(modalMail.getKeterangan());
                modalMailDto.setPerihalSurat(modalMail.getPerihalSurat());
                modalMailDto.setLampiran(modalMail.getLampiran());
                modalMailDto.setStatusApproveDesc(statusApproveDto.getDescription());
                modalMailDto.setStatusApprove(modalMail.getStatusApprove());
                modalMailDto.setLainnya(modalMail.getLainnya());
                modalMailDto.setUploadId(modalMail.getUploadId());
                modalMailDtos.add(modalMailDto);
            }
            response.setData(modalMailDtos);
            response.setIsSuccess(true);
            log.info("View Mail Succesfull", mModalMails);
        }catch (Exception e){
            log.error("View Mail Failed", e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public LoadModalMailNeededApprovedByHeadResponse loadModalMailNeededApprovedByHead(LoadModalMailNeededApprovedByHeadRequest request) {
        LoadModalMailNeededApprovedByHeadResponse response = new LoadModalMailNeededApprovedByHeadResponse();
        try {
            MModalMail mModalMail = this.mModalMailRepository.findById(request.getId()).orElse(null);
            if(mModalMail == null)
                throw new Exception("Data mail doesn't exist with id: " + request.getId());
            List<StatusApproveDto> statusApproveDtos = CommonUtils.getStatusApproveOption();
            StatusApproveDto statusApproveDto = statusApproveDtos.stream().filter(item -> item.getCode() == mModalMail.getStatusApprove()).findAny().orElse(null);
            ModalMailDto modalMailDto = new ModalMailDto();
            modalMailDto.setId(mModalMail.getId());
            modalMailDto.setStatusApproveDesc(statusApproveDto.getDescription());
            modalMailDto.setStatusApprove(mModalMail.getStatusApprove());
            modalMailDto.setNoSurat(mModalMail.getNoSurat());
            modalMailDto.setAsalSurat(mModalMail.getAsalSurat());
            modalMailDto.setTanggalSurat(mModalMail.getTanggalSurat());
            modalMailDto.setPerihalSurat(mModalMail.getPerihalSurat());
            response.setData(modalMailDto);
            response.setIsSuccess(true);
        }catch (Exception e){
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @Override
    public ViewModalMailApprovedByHeadResponse viewModalMailApprovedByHead(ViewModalMailApprovedByHeadRequest request) {
        ViewModalMailApprovedByHeadResponse response = new ViewModalMailApprovedByHeadResponse();
        try {
            List<MModalMail> mModalMails = this.mModalMailRepository.findAllByMailApproveByHead(CommonConstant.STATUS_APPROVE_APPROVED);
            List<ModalMailDto> modalMailDtosTataUsaha = new ArrayList<>();
            List<ModalMailDto> modalMailDtosP2M = new ArrayList<>();
            List<ModalMailDto> modalMailDtosRehabilitasi = new ArrayList<>();
            List<ModalMailDto> modalMailDtosBrantas = new ArrayList<>();
            List<StatusApproveDto> statusApproveDtos = CommonUtils.getStatusApproveOption();
            for (MModalMail modalMail:mModalMails){
                StatusApproveDto statusApproveDto = statusApproveDtos.stream().filter(item -> item.getCode() == modalMail.getStatusApprove()).findAny().orElse(null);
                ModalMailDto modalMailDto = new ModalMailDto();
                modalMailDto.setId(modalMail.getId());
                modalMailDto.setNoSurat(modalMail.getNoSurat());
                modalMailDto.setTanggalSurat(modalMail.getTanggalSurat());
                modalMailDto.setTanggalDiterima(modalMail.getTanggalDiterima());
                modalMailDto.setTujuanSurat(modalMail.getTujuanSurat());
                modalMailDto.setAsalSurat(modalMail.getAsalSurat());
                modalMailDto.setKeterangan(modalMail.getKeterangan());
                modalMailDto.setPerihalSurat(modalMail.getPerihalSurat());
                modalMailDto.setLampiran(modalMail.getLampiran());
                modalMailDto.setStatusApproveDesc(statusApproveDto.getDescription());
                modalMailDto.setStatusApprove(modalMail.getStatusApprove());
                modalMailDto.setLainnya(modalMail.getLainnya());
                modalMailDto.setUploadId(modalMail.getUploadId());
                if (modalMail.getDisposisiKategori().equals(CommonConstant.DISPOSITION_CATEGORY_TATA_USAHA)){
                    modalMailDtosTataUsaha.add(modalMailDto);
                }else if (modalMail.getDisposisiKategori().equals(CommonConstant.DISPOSITION_CATEGORY_P2M)){
                    modalMailDtosP2M.add(modalMailDto);
                }else if (modalMail.getDisposisiKategori().equals(CommonConstant.DISPOSITION_CATEGORY_REHABILITASI)){
                    modalMailDtosRehabilitasi.add(modalMailDto);
                }else if (modalMail.getDisposisiKategori().equals(CommonConstant.DISPOSITION_CATEGORY_BRANTAS)){
                    modalMailDtosBrantas.add(modalMailDto);
                }
            }
            response.setModalMailDtoTataUsahaList(modalMailDtosTataUsaha);
            response.setModalMailDtoP2MList(modalMailDtosP2M);
            response.setModalMailDtoRehabilitasiList(modalMailDtosRehabilitasi);
            response.setModalMailDtoBrantasList(modalMailDtosBrantas);
            response.setIsSuccess(true);
            log.info("View Mail Approve Succesfull", mModalMails);
        }catch (Exception e){
            log.error("View Mail Approve Failed", e.getMessage());
            e.printStackTrace();
            response.setIsSuccess(false);
            response.setMessage(e.getMessage());
        }
        return response;
    }
}

