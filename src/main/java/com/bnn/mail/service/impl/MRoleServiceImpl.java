package com.bnn.mail.service.impl;

import com.bnn.mail.model.MRole;
import com.bnn.mail.repository.MRoleRepository;
import com.bnn.mail.service.MRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
public class MRoleServiceImpl implements MRoleService {

    private final MRoleRepository mRoleRepository;

    public  MRoleServiceImpl(MRoleRepository mRoleRepository) {
        this.mRoleRepository = mRoleRepository;
    }
}
