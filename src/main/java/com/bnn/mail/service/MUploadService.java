package com.bnn.mail.service;

import com.bnn.mail.dto.FileDto;
import com.bnn.mail.dto.UserSessionDto;
import com.bnn.mail.model.MUpload;
import org.springframework.web.multipart.MultipartFile;

public interface MUploadService {
    MUpload insertUpload(MultipartFile file, UserSessionDto sessionDto);
    FileDto viewByUploadId(String uploadId);

}
