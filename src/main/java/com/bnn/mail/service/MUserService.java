package com.bnn.mail.service;

import com.bnn.mail.dto.request.SignUpRequest;
import com.bnn.mail.dto.response.SignupResponse;

public interface MUserService {
    SignupResponse insertUser(SignUpRequest request);
}
