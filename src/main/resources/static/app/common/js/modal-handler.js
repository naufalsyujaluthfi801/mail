function openModal(code, module) {
    let stringCode = getMessage(code);
    let stringModule = getMessage( module);
    let bodyContent = getMessage('common.message.0008');
    bodyContent = bodyContent.replace('{0}', stringCode.toUpperCase());
    bodyContent = bodyContent.replace('{1}', stringModule.toLowerCase());

    $('#backdropModal').modal('toggle');
    $('p.body-content').html(bodyContent);
}

function openModalForSubmitForm(code, module, buttonObj) {
    openModal(code, module);
    $('#yes-confirmation').unbind('click');
    $('#yes-confirmation').click(function () {
        $('#yes-confirmation').attr('disabled', true);
        $('#no-confirmation').attr('disabled', true);
        let form = $(buttonObj).closest('form');
        if (form.length > 0) {
            form.submit();
        }
    });
}