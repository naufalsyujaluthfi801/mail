$(function() {
    setActiveLeftSideBar();
    setActivePagination();
});

function setActiveLeftSideBar() {
    var url = window.location.pathname + '/x';
    var menu;
    do {
        url = url.replace('/' + url.split('/')[url.split('/').length - 1], '')
        menu = document.querySelectorAll("a[href*='" + url + "']");
    } while(menu.length < 1);

    var liMenu = $(menu[0]).parent();
    liMenu.addClass("active");
    var parents = liMenu.parents();
    for (var i = 0; i < parents.length; i++) {
        if($(parents[i]).is('li'))
            $(parents[i]).addClass("show");
    }
}

function setActivePagination() {
    var requestParam = window.location.search;
    var pageRequestParam = requestParam.split('?page=')[1];
    var page = !requestParam || requestParam.length === 0 ? 1 : pageRequestParam;
    var ulPagination = document.querySelectorAll('nav.paging ul');
    if (ulPagination.length > 0) {
        var liPagination = ulPagination[0].querySelectorAll('li.page-item.content');
        for (var i = 0; i < liPagination.length; i++) {
            if (i === page - 1) {
                if($(liPagination[i]).is('li'))
                    $(liPagination[i]).addClass("active");
            }
        }
    }
}