$(document).ready(function() {
    $('.open-modal-acc-laporan').click(function(){
        //clone dialog and remove ids to ensure uniqueness
        let $modal = $('#accLaporanModal').clone().removeAttr('id');

        //apply custom values where needed
        let $btn = $(this);
        let rowId = $btn.attr('data-id');
        let noSurat = $btn.attr('data-no-surat');
        let tanggalSurat = $btn.attr('data-tanggal-surat');
        let tanggalDiterima = $btn.attr('data-tanggal-diterima');
        let asalSurat = $btn.attr('data-asal-surat');
        let tujuanSurat = $btn.attr('data-tujuan-surat');
        let keterangan = $btn.attr('data-keterangan');
        let perihalSurat = $btn.attr('data-perihal-surat');

        $modal.find('#noSurat').val(noSurat);
        $modal.find('#tanggalSurat').val(tanggalSurat);
        $modal.find('#tanggalDiterima').val(tanggalDiterima);
        $modal.find('#asalSurat').val(asalSurat);
        $modal.find('#tujuanSurat').val(tujuanSurat);
        $modal.find('#keterangan').val(keterangan);
        $modal.find('#perihalSurat').val(perihalSurat);

        $modal.find('[name="id"]').val(rowId);
        $modal.find('form').attr('action').replace('_id_', rowId);
        // $modal.find('#submitModal').attr('href', $modal.find('#submitModal').attr('href').replace('_id_', rowId));
        $modal.html();
        $modal.modal('show');
    });
});