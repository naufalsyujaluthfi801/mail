
CREATE TABLE "m_employee" (
                              "id" VARCHAR(50) NOT NULL,
                              "name" VARCHAR(50) NOT NULL,
                              "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                              "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                              "created_date" TIMESTAMP NULL DEFAULT NULL,
                              "created_by" VARCHAR(50) NULL DEFAULT NULL,
                              "updated_date" TIMESTAMP NULL DEFAULT NULL,
                              "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                              PRIMARY KEY ("id")
);