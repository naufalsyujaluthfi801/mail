INSERT INTO m_role(id, NAME, code, is_active, is_deleted, created_date)
VALUES
('ab0b31ad-d793-43b4-87fd-65a1c479d778', 'pegawai', 'PGM', TRUE, FALSE, NOW());

INSERT INTO m_permission (id, NAME, code, created_date)
VALUES
    ('9714f2db-c447-4248-ba5b-d56ba4943f48', 'Dashboard sidebar', 'dashboard.sidebar.permission', NOW());

INSERT INTO m_role_permission (id, role_id, permission_id)
VALUES
('5e911fa4-8a67-4057-9ba6-0cc3cdb27bff', 'ab0b31ad-d793-43b4-87fd-65a1c479d778', '9714f2db-c447-4248-ba5b-d56ba4943f48');