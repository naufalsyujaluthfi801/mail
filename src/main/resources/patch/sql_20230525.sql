CREATE TABLE "m_modal_mail" (
                                "id" VARCHAR(50) NOT NULL,
                                "no_surat" VARCHAR(50) NOT NULL,
                                "tanggal_surat" VARCHAR(50) NOT NULL,
                                "tanggal_diterima" VARCHAR(50) NOT NULL,
                                "date" DATE NOT NULL,
                                "asal_surat" VARCHAR NOT NULL,
                                "tujuan_surat" VARCHAR NOT NULL,
                                "keterangan" TEXT NOT NULL,
                                "perihal" VARCHAR NOT NULL,
                                "lampiran" VARCHAR(1) NOT NULL,
                                "lainnya" VARCHAR NULL,
                                "upload_id" VARCHAR(50) NOT NULL,
                                "is_active" BOOLEAN NOT NULL DEFAULT 'true',
                                "is_deleted" BOOLEAN NOT NULL DEFAULT 'false',
                                "created_date" TIMESTAMP NULL DEFAULT NULL,
                                "created_by" VARCHAR(50) NULL DEFAULT NULL,
                                "updated_date" TIMESTAMP NULL DEFAULT NULL,
                                "updated_by" VARCHAR(50) NULL
);