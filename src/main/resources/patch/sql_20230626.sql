CREATE TABLE "m_upload" (
                            "id" VARCHAR(50) NOT NULL,
                            "filename" VARCHAR NOT NULL,
                            "filepath" VARCHAR NOT NULL,
                            "filesize" BIGINT NOT NULL,
                            "filetype" VARCHAR(100) NOT NULL,
                            "created_date" TIMESTAMP NULL DEFAULT NULL,
                            "created_by" VARCHAR(50) NULL DEFAULT NULL,
                            "uploaded_date" TIMESTAMP NULL DEFAULT NULL,
                            "uploaded_by" VARCHAR(50) NULL DEFAULT NULL,
                            PRIMARY KEY ("id")
);