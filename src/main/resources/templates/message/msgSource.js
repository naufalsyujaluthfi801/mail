var messages = /*[[${messages}]]*/'default';
function getMessage(key) {
    if (messages[key]) {
        return messages[key];
    }
    return key;
}
